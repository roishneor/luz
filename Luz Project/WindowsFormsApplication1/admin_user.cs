﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class admin_user
    {
        // Class members: 
        // Property. 
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        //method 
        private void validate()
        {
            //user name validate
            string name_pattern = "\\w+";
            if (System.Text.RegularExpressions.Regex.IsMatch(this.name, name_pattern))
            {
                System.Console.WriteLine(this.name + " - valid");
            }
            else
            {
                System.Console.WriteLine(this.name + " - invalid");
            }
            //user password validate
            string psw_pattern = "\\w+";
            if (System.Text.RegularExpressions.Regex.IsMatch(this.password, psw_pattern))
            {
                System.Console.WriteLine(this.password + " - valid");
            }
            else
            {
                System.Console.WriteLine(this.password + " - invalid");
            }

            //user name validate
            string email_pattern = "\\w+@\\w+\\.\\w+(\\.\\w)?";
            if (System.Text.RegularExpressions.Regex.IsMatch(this.email, email_pattern))
            {
                System.Console.WriteLine(this.email + " - valid");
            }
            else
            {
                System.Console.WriteLine(this.email + " - invalid");
            }

        }
        public admin_user(string name, string password, string email)
        {
            this.name = name;
            this.password = password;
            this.email = email;
            validate();
        }
    }
}
