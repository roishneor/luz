﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Collections.Concurrent;
using System.Threading;
using DevComponents.Schedule.Model;

namespace WindowsFormsApplication1
{
    class Utils
    {
        public static ConcurrentQueue<MailRequest> MailsQueue;
        static Thread MailThread;

        public static void initMailThread()
        {
            MailsQueue = new ConcurrentQueue<MailRequest>();
            MailThread = new Thread(sendMailsInQueue);
            MailThread.IsBackground = true;
            MailThread.Start();
        }

        private static void sendMailsInQueue(object obj)
        {
            try
            {
                while (true)
                {
                    while (!MailsQueue.IsEmpty)
                    {
                        MailRequest req;
                        if (MailsQueue.TryDequeue(out req))
                        {
                            string body = File.ReadAllText(req.BodyFilePath);

                            //replace all the replacements:
                            body = body.Replace("!FIRSTNAME!", req.FirstName);
                            body = body.Replace("!TREATMENT!", req.Treatment);
                            body = body.Replace("!TREATMENTDATE!", req.TreatmentDate.ToShortDateString());
                            body = body.Replace("!TREATMENTTIME!", req.TreatmentDate.ToShortTimeString());
                            body = body.Replace("!TREATMENTLENGTH!", req.TreatmentLenght.ToString());

                            sendMail(req.Subject, req.ToList, body, req.isHtml);
                        }
                    }
                    sendMailsForTomorrowsApp();
                    backup();
                    Thread.Sleep(10000);
                }
            }
            catch (Exception e)
            {
                handleException(e);
            }
        }

        private static void backup()
        {
            string fileName = "LastBackupDate.txt";
            try
            {
                
                if (!File.Exists(fileName) || DateTime.Parse(File.ReadAllText(fileName).Trim()) < DateTime.Now.AddDays(-7))
                {
                    string dest = @"C:\CosmeticsBackup\CosmeticsDB_" + DateTime.Now.ToShortDateString().Replace('/', '.') + ".mdb";
                    if (!Directory.Exists(Path.GetDirectoryName(dest)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(dest));
                    }
                    File.Copy("CosmeticsDB.mdb", dest,true);
                    if (sendMail("Cosmetics Backup", "roishneor@gmail.com", "Attached DataBase backup from date: " + DateTime.Now.ToString(), false, dest))
                    {
                        File.WriteAllText(fileName, DateTime.Now.ToShortDateString());
                        MessageBox.Show("גיבוי נתונים נשלח בהצלחה");
                    }
                    else
                    {
                        throw new Exception("קובץ הגיבוי נשמר בתקייה מקומית, אבל התרחשה שגיאה בעת נסיון שליחת דוא\"ל");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("שגיאה בעת ניסיון לבצע גיבוי לנתונים:\n" + e.Message);
                File.Delete(fileName);
            }
        }

        private static void sendMailsForTomorrowsApp()
        {
            string fileName = "LastSentRemindersDate.txt";
            int minHour = 10;
            int MaxHour = 17;
            if (DateTime.Now.Hour < minHour || DateTime.Now.Hour > MaxHour)
            {
                return;
            }
            if (File.Exists(fileName) && File.ReadAllText(fileName).Equals(DateTime.Now.ToShortDateString()))
            {
                return;
            }

            File.Delete(fileName);
            List<MyAppointment> list = getAppFromDB(DateTime.Now.AddDays(1), DateTime.Now.AddDays(1));
            //AppointmentList list = 

            foreach (MyAppointment app in list)
            {
                if (app.SubmitDate.ToShortDateString() != DateTime.Now.ToShortDateString()) //to make sure it wasn't schedualed today
                {
                    Client client = Utils.getClientFromDB(app.ClientName);
                    if (client != null)
                    {
                        MailRequest mr = new MailRequest();
                        mr.Subject = "תזכורת עבור טיפול שנקבע עבורך למחר";
                        mr.BodyFilePath = "Mail\\MailTemplates\\AddNewAppointment.html";
                        mr.FirstName = client.FirstName;
                        mr.LastName = client.LastName;
                        mr.ToList.Add(client.eMail);
                        mr.isHtml = true;
                        mr.Treatment = app.Treatment;
                        mr.TreatmentDate = app.StartTime;
                        mr.TreatmentTime = app.StartTime;
                        mr.TreatmentLenght = app.Minutes;
                        Utils.MailsQueue.Enqueue(mr);
                    }
                }
            }

            File.WriteAllText(fileName, DateTime.Now.ToShortDateString());
        }

        private static OleDbConnection _connection = null;

        public static OleDbConnection getConnection()
        {
            if (_connection != null && _connection.State.Equals(ConnectionState.Open))
            {
                return _connection;
            }

            string connetionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=CosmeticsDB.mdb;";
            _connection = new OleDbConnection(connetionString);
            try
            {
                _connection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection to DB! \n" + ex.Message);
                return null;
            }
            return _connection;
        }

        public static string dr2str(OleDbDataReader dr)
        {
            string res = "";
            for (int i = 0; i < dr.FieldCount; i++)
            {
                res += dr.GetName(i).ToString() + ": ";
                res += dr[i].ToString();
                if (i != dr.FieldCount - 1)
                {
                    res += ", ";
                }
            }
            return res;
        }

        public static Client getClientFromDB(string fullName)
        {
            try
            {
                Client client = null;
                string command = "SELECT * FROM Clients WHERE Clients.[FirstName] +' '+ Clients.[LastName]= '" + fullName + "';";
                OleDbCommand cmd = new OleDbCommand(command, getConnection());
                OleDbDataReader dr = null;
                try
                {
                    dr = cmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error: " + e.Message);
                }

                if (dr != null && dr.Read())
                {
                    client = new Client();
                    client.ID = dr.GetInt32((dr.GetOrdinal("ID")));
                    client.FirstName = dr[(dr.GetOrdinal("FirstName"))].ToString();
                    client.LastName = dr[(dr.GetOrdinal("LastName"))].ToString();
                    client.CellPhone = dr[(dr.GetOrdinal("CellPhone"))].ToString();
                    client.HomePhone = dr[(dr.GetOrdinal("HomePhone"))].ToString();
                    client.eMail = dr[(dr.GetOrdinal("eMail"))].ToString();
                    client.BirthDay = dr.GetDateTime((dr.GetOrdinal("BirthDay")));
                    client.JoinDate = dr.GetDateTime((dr.GetOrdinal("JoinDate")));
                }
                dr.Dispose();
                cmd.Dispose();
                return client;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<MyAppointment> getAppFromDB(DateTime start, DateTime end)
        {
            string hFormat = "MM/dd/yyyy";
            string starts = start.ToString(hFormat);
            string ends = end.ToString(hFormat);

            List<MyAppointment> list = new List<MyAppointment>();

            string command = "SELECT * FROM Appointments WHERE (((Appointments.ApDate)>=#" + starts + "#) and ((Appointments.ApDate)<=#" + ends + "#));";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = null;
            try
            {
                dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }

            while (dr != null && dr.Read())
            {
                MyAppointment app = new MyAppointment();
                app.ID = dr.GetInt32((dr.GetOrdinal("ID")));
                app.ClientName = dr[(dr.GetOrdinal("Client"))].ToString();
                app.StartTime =dr.GetDateTime((dr.GetOrdinal("ApDate"))).AddHours(dr.GetDateTime((dr.GetOrdinal("ApTime"))).Hour).AddMinutes(dr.GetDateTime((dr.GetOrdinal("ApTime"))).Minute);
                app.EndTime = app.StartTime.AddMinutes(dr.GetInt32(dr.GetOrdinal("ApLength")));
                app.Minutes = dr.GetInt32(dr.GetOrdinal("ApLength"));
                app.Treatment = dr[(dr.GetOrdinal("Treatment"))].ToString();
                app.Comment = dr[(dr.GetOrdinal("Comment"))].ToString();
                app.Subject = getAppSubject(app);
                app.Payed = dr.GetBoolean(dr.GetOrdinal("Payed"));
                app.PayedSum = dr.GetInt32((dr.GetOrdinal("PayedSum")));
                app.ColorBlockBrush = getBrushForTreatment(app.Treatment);
                app.SubmitDate = dr.GetDateTime((dr.GetOrdinal("SubmitDate")));
                list.Add(app);
            }
            dr.Dispose();
            cmd.Dispose();
            return list;
        }

        public static string getAppSubject(MyAppointment app)
        {
            return app.ClientName + ", " + app.Treatment;
        }

        public static void editExpensesInDB(int ID, int Summ, string ForWhat, string Comment)
        {
            string command = "UPDATE Expenses SET Summ=" + Summ.ToString() +
                ", ForWhat='" + ForWhat +
                "', Comment='" + Comment.ToString() +
                "' WHERE ID=" + ID + ";";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void editRevenueInDB(int ID, int Summ, string ClientName, string ForWhat, string Comment)
        {
            string command = "UPDATE Revenues SET Summ=" + Summ.ToString() +
                ", ClientName='" + ClientName +
                "', ForWhat='" + ForWhat +
                "', Comment='" + Comment.ToString() +
                "' WHERE ID=" + ID + ";";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void editClientInDB(Client client)
        {
            string hFormat = "MM/dd/yyyy";
            string bDay = client.BirthDay.ToString(hFormat);

            string command = "UPDATE Clients SET FirstName='" + client.FirstName +
                "', LastName='" + client.LastName +
                "', CellPhone='" + client.CellPhone +
                "', HomePhone='" + client.HomePhone +
                "', eMail='" + client.eMail +
                //"', JoinDate=#" + DateTime.Now.ToShortDateString() +
                "', BirthDay=#" + bDay +
                "# WHERE ID=" + client.ID + ";";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void addRevenueToDB(int sum, string client, string forWhat, string comment)
        {
            string hFormat = "MM/dd/yyyy";
            string submit = DateTime.Now.ToString(hFormat);

            string command = "INSERT INTO Revenues (Summ, ClientName, ForWhat, Comment,SubmitDate) VALUES (" +
                "" + sum.ToString() + ", " +
                "'" + client + "', " +
                "'" + forWhat + "', " +
                "'" + comment + "', " +
                "#" + submit + "#);";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void addExpenseToDB(int sum, string forWhat, string comment)
        {
            string hFormat = "MM/dd/yyyy";
            string starts = DateTime.Now.ToString(hFormat);

            string command = "INSERT INTO Expenses (Summ, ForWhat, Comment,SubmitDate) VALUES (" +
                "" + sum.ToString() + ", " +
                "'" + forWhat + "', " +
                "'" + comment + "', " +
                "#" + starts + "#);";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void AddClientToDB(Client client)
        {
            string hFormat = "MM/dd/yyyy";
            string join = DateTime.Now.ToString(hFormat);
            string bDay = client.BirthDay.ToString(hFormat);

            string command = "INSERT INTO Clients (FirstName, LastName, CellPhone, HomePhone, eMail, JoinDate, BirthDay) VALUES (" +
                "'" + client.FirstName + "', " +
                "'" + client.LastName + "', " +
                "'" + client.CellPhone + "', " +
                "'" + client.HomePhone + "', " +
                "'" + client.eMail + "', " +
                "#" + join + "#, " +
                "#" + bDay + "#);";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void editAppointmentInDB(MyAppointment app)
        {
            string hFormat = "MM/dd/yyyy";
            string starts = app.StartTime.ToString(hFormat);
            string ends = app.EndTime.ToString(hFormat);

            string command = "UPDATE Appointments SET Client='" + app.ClientName +
                "', ApDate=#" + starts +
                "#, ApTime=#" + app.StartTime.ToShortTimeString() +
                "#, Treatment='" + app.Treatment +
                "', ApLength=" + app.Minutes +
                ", Comment='" + app.Comment +
                "', Payed=" + app.Payed.ToString() +
                ", PayedSum=" + app.PayedSum.ToString() +
                " WHERE ID=" + app.ID + ";";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void AddAppointmentToDB(MyAppointment app)
        {
            string hFormat = "MM/dd/yyyy";
            string starts = app.StartTime.ToString(hFormat);
            string submits = DateTime.Now.ToString(hFormat);

            string command = "INSERT INTO Appointments (Client, ApDate, ApTime, Treatment, SubmitDate, SubmitTime, ApLength, Comment,Payed,PayedSum) VALUES (" +
                "'" + app.ClientName + "'," +
                "#" + starts + "#," +
                "#" + app.StartTime.ToShortTimeString() + "#," +
                "'" + app.Treatment + "'," +
                "#" + submits + "#," +
                "#" + DateTime.Now.ToShortTimeString() + "#," +
                "" + app.Minutes + "," +
                "'" + app.Comment + "'," +
                app.Payed.ToString() + "," +
                app.PayedSum.ToString() + ");";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static string[] getClientList()
        {
            List<string> list = new List<string>();
            string command = "SELECT * FROM Clients;";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = null;
            try
            {
                dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }

            while (dr != null && dr.Read())
            {
                string name = dr[(dr.GetOrdinal("FirstName"))].ToString() + " " + dr[(dr.GetOrdinal("LastName"))].ToString();
                list.Add(name);
            }
            dr.Dispose();
            cmd.Dispose();
            return list.ToArray(); ;
        }

        public static string[] getTreatmentNamesList()
        {
            List<Treatment> tList = getTreatmentList();
            List<string> nameList = new List<string>();
            foreach (Treatment t in tList)
            {
                nameList.Add(t.Name);
            }
            return nameList.ToArray();
        }

        public static List<Treatment> getTreatmentList()
        {
            List<Treatment> list = new List<Treatment>();
            string command = "SELECT * FROM Treatments;";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = null;
            try
            {
                dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }

            while (dr != null && dr.Read())
            {
                Treatment t = new Treatment();
                t.ID = dr.GetInt32(dr.GetOrdinal("ID"));
                t.Price = dr.GetInt32(dr.GetOrdinal("Price"));
                t.Minutes = dr.GetInt32(dr.GetOrdinal("Minutes"));
                t.Name = dr[(dr.GetOrdinal("TreatmentName"))].ToString();
                list.Add(t);
            }
            dr.Dispose();
            cmd.Dispose();
            return list; ;
        }

        public static int getTreatmentPrice(string treatment)
        {
            string command = "SELECT * FROM Treatments;";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = null;
            try
            {
                dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }

            while (dr != null && dr.Read())
            {
                string name = dr[(dr.GetOrdinal("TreatmentName"))].ToString();
                if (name.Equals(treatment, StringComparison.InvariantCultureIgnoreCase))
                {
                    int price = dr.GetInt32(dr.GetOrdinal("Price"));
                    dr.Dispose();
                    cmd.Dispose();
                    return price;
                }
            }
            dr.Dispose();
            cmd.Dispose();
            return 0;
        }

        public static System.Drawing.Brush getBrushForTreatment(string treatment)
        {
            string command = "SELECT * FROM Treatments;";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = null;
            try
            {
                dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e.Message);
            }

            while (dr != null && dr.Read())
            {
                string name = dr[(dr.GetOrdinal("TreatmentName"))].ToString();
                if (name.Equals(treatment, StringComparison.InvariantCultureIgnoreCase))
                {
                    int id = dr.GetInt32(dr.GetOrdinal("ID"));
                    switch (id % 10)
                    {
                        case 0:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.AliceBlue;
                        case 1:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Purple;
                        case 2:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Brown;
                        case 3:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Chartreuse;
                        case 4:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Cyan;
                        case 5:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.DeepPink;
                        case 6:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.DimGray;
                        case 7:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Firebrick;
                        case 8:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Fuchsia;
                        case 9:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Gold;
                        default:
                            dr.Dispose();
                            cmd.Dispose();
                            return System.Drawing.Brushes.Transparent;
                    }
                }
            }
            dr.Dispose();
            cmd.Dispose();
            return System.Drawing.Brushes.Transparent;
        }

        public static void DeleteAppFromDB(MyAppointment app)
        {
            string command = "DELETE FROM Appointments" +
                " WHERE ID=" + app.ID + ";";
            OleDbCommand cmd = new OleDbCommand(command, getConnection());
            OleDbDataReader dr = cmd.ExecuteReader();
            if (dr.RecordsAffected != 1)
            {
                throw new Exception("The DB was not updated");
            }
            dr.Dispose();
            cmd.Dispose();
        }

        public static void sendMail(string subject, List<string> toList, string bodyPath, bool html = true, string attach = "")
        {
            foreach (string address in toList)
            {
                if (address.Trim() != "")
                    sendMail(subject, address, bodyPath, html);
            }
        }

        public static bool sendMail(string subject, string address, string body, bool html = true, string attach = "")
        {
            try
            {
                if (!bool.Parse(File.ReadAllText("AllowMails.txt")) && address != "roishneor@gmail.com") //if mails are not allowed
                {
                    return true;
                }

                MailMessage msg = new MailMessage();
                string[] myMail = File.ReadAllLines("Mail\\MyMail.txt"); //myMail[0] = username, myMail[1] = password

                msg.From = new MailAddress(myMail[0]);
                msg.To.Add(address);

                string[] bcc = File.ReadAllLines("Mail\\bcc.txt");
                foreach (string line in bcc)
                {
                    if (line.Trim() != "")
                    {
                        msg.Bcc.Add(line);
                    }

                }

                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = html;

                if (!(attach == "") && File.Exists(attach))
                {
                    Attachment att = new Attachment(attach);
                    msg.Attachments.Add(att);
                }

                SmtpClient client = new SmtpClient();

                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(myMail[0], myMail[1]);
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(msg);

            }
            catch (Exception e)
            {
                MessageBox.Show("Error: can't sent mail. " + e.Message);
                return false;
            }
            return true;
        }

        public static void handleException(Exception e)
        {
            string err = DateTime.Now.ToString() + "\n\n" +
                "Message: " + e.Message + "\n\n" +
                e.ToString();
            File.WriteAllText("LastError.txt", err);
            try
            {
                sendMail("Error in Cosmetics", "roishneor@gmail.com", err, false);
                _connection.Close();
                _connection = null;
            }
            catch (Exception)
            {

            }
            MessageBox.Show("קרתה שגיאה והתוכנית חייבת להסגר \n אל תתקשרי לרועי, אין לו כוח אלייך \n:)", "אופס קרתה שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
