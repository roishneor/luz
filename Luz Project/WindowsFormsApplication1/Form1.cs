﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;





namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {
        public static int forImg = 0;
        private Image oimg,rimg;
        private bool rotate = true;
        string new_email;
        string new_us;
        string new_pwd;



        public Form1()
        {
            InitializeComponent();
            timer.Start();
            this.oimg = wheel_icon.Image;
            this.rimg = RotateImage(this.wheel_icon.Image, 30);
            this.groupBox1.Hide();
            this.groupBox2.Hide();

            #if DEBUG

            this.Hide();
            office_view new_form = new office_view();
            new_form.ShowDialog();

            #endif
        }

    
        private void timer_Tick(object sender, EventArgs e)
        {
            progress_bar.Increment(1);
            int pbv = this.progress_bar.Value;
            if (this.rotate && pbv % 2  == 0 ) { this.wheel_icon.Image = chooseImage(this.oimg, this.rimg); }

            if      (pbv > 10 && pbv <= 30)  { this.loading_label.Text = "Loading..."; }
            else if (pbv > 30 && pbv <= 60)  { this.loading_label.Text = "Connecting To Server..."; }
            else if (pbv > 60 && pbv <= 99)  { this.loading_label.Text = "Updateying Data Base..."; }
            else if (pbv == 100 )
            {
                this.loading_label.Text = "App Is Ready To Use.";
                this.rotate = false;
                this.wheel_icon.Image = this.oimg;
            }
        }

        /// <summary>
        /// method to rotate an image either clockwise or counter-clockwise
        /// </summary>
        /// <param name="img">the image to be rotated</param>
        /// <param name="rotationAngle">the angle (in degrees).
        /// NOTE: 
        /// Positive values will rotate clockwise
        /// negative values will rotate counter-clockwise
        /// </param>
        /// <returns></returns>
        public static Image RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.groupBox1.Hide();
            this.groupBox2.Show();
        }

        private void close_button_MouseHover(object sender, EventArgs e)
        {
            close_button.ForeColor = Color.Blue;
        }

        private void close_button_MouseLeave(object sender, EventArgs e)
        {
            close_button.ForeColor = Color.Black;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            new_us = this.unna_txt_box.Text;
        }

        private void pna_txt_box_TextChanged(object sender, EventArgs e)
        {
            new_pwd = this.pna_txt_box.Text;
        }

        private void ena_txt_box_TextChanged(object sender, EventArgs e)
        {
            new_email = this.ena_txt_box.Text;
        }

        private void login_Click(object sender, EventArgs e)
        {
            this.groupBox2.Hide();
            this.groupBox1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == "Admin")
            {
                this.Hide();
                office_view new_form = new office_view();
                new_form.ShowDialog();
                return;
            }

            admin_user new_acount = new admin_user(new_us, new_pwd,new_email);
            MessageBox.Show(new_email+" "+new_pwd + " " + new_us);
            upadte_data_base(new_acount);
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.groupBox2.Hide();
        }

        public void upadte_data_base(admin_user user)
        {

        }

        /// set the image to one of two option base on current display
        private Image chooseImage( Image img1,Image img2)
        {
            Image new_img;
            switch (forImg % 3)
            {
                case 0:
                    new_img = img1;
                    break;
                    
                case 1:
                    new_img = img2;
                    break;
                default:
                    new_img = RotateImage(img2,30);
                    break;
            }
            forImg = 1 ^ forImg;
            return new_img;
        }
    }

  
}



