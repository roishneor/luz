﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class MailRequest
    {
        public MailRequest()
        {
            ToList = new List<string>();
        }

        public string Subject;
        public string BodyFilePath;
        public List<string> ToList;
        public bool isHtml;
        public string FirstName;
        public string LastName;
        public string Treatment;
        public DateTime TreatmentDate;
        public DateTime TreatmentTime;
        public int TreatmentLenght;

    }
}
