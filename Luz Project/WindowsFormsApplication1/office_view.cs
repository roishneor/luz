﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevComponents.DotNetBar;

namespace WindowsFormsApplication1
{
    public partial class office_view : Office2007Form
    {
        public office_view()
        {
            InitializeComponent();
            monthCalendarAdv1.DisplayMonth = DateTime.Now;
        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            new_customer nc = new new_customer();
            nc.ShowDialog();
        }

        private void radioDay_CheckedChanged(object sender, EventArgs e)
        {
            radio_CheckedChanged();
        }

        private void radio_CheckedChanged()
        {
            panelday.Show();
            calendarView1.DateSelectionStart = DateTime.Now;
            if (radioDay.Checked)
            {
                calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Day;
            }
            else if (radioWeek.Checked)
            {
                calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Week;
            }
            else if (radioMonth.Checked)
            {
                calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Month;
            }
        }

        private void radioWeek_CheckedChanged(object sender, EventArgs e)
        {
            radio_CheckedChanged();
        }

        private void radioMonth_CheckedChanged(object sender, EventArgs e)
        {
            radio_CheckedChanged();
        }

        private void calendarView1_MouseWheel(object sender, MouseEventArgs e)
        {
            if (ModifierKeys == Keys.Control)
            {
                if (e.Delta > 0) //zoom in
                {
                    zoomInCalendar();
                }
                else //zoom out
                {
                    zoomOutCalendar();
                }
            }

        }

        private void zoomOutCalendar()
        {
            DevComponents.DotNetBar.Schedule.eCalendarView view = calendarView1.SelectedView;
            switch (view)
            {
                case DevComponents.DotNetBar.Schedule.eCalendarView.Day:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Week;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.Month:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Year;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.None:
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.TimeLine:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Day;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.Week:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Month;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.Year:
                    break;
                default:
                    break;
            }
        }

        private void zoomInCalendar()
        {
            DevComponents.DotNetBar.Schedule.eCalendarView view = calendarView1.SelectedView;
            switch (view)
            {
                case DevComponents.DotNetBar.Schedule.eCalendarView.Day:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.TimeLine;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.Month:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Week;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.None:
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.TimeLine:
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.Week:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Day;
                    break;
                case DevComponents.DotNetBar.Schedule.eCalendarView.Year:
                    calendarView1.SelectedView = DevComponents.DotNetBar.Schedule.eCalendarView.Month;
                    break;
                default:
                    break;
            }
        }

        private void addNewApp_Click(object sender, EventArgs e)
        {
            //calendarView1_ItemClick(null, e);
            ToolStripMenuItem item = (ToolStripMenuItem)sender;

            DateTime start;
            DateTime end;

            if (calendarView1.DateSelectionStart == null || calendarView1.DateSelectionEnd == null)
            {
                start = DateTime.Now;
                end = DateTime.Now.AddMinutes(30);
            }
            else
            {
                start = (DateTime)calendarView1.DateSelectionStart;
                end = (DateTime)calendarView1.DateSelectionEnd;
            }

            new_treatment nt_form = new new_treatment();

            nt_form.dtpStartDate.Value = start.Date;
            nt_form.dtpStartTime.Value = start;

            nt_form.dtpEndDate.Value = end.Date;
            nt_form.dtpEndTime.Value = end;

            nt_form.ShowDialog();

            if (nt_form.DialogResult.Equals(DialogResult.OK))
            {
                start = nt_form.dtpStartDate.Value.Date.AddHours(nt_form.dtpStartTime.Value.Hour).AddMinutes(nt_form.dtpStartTime.Value.Minute);
                end = nt_form.dtpEndDate.Value.Date.AddHours(nt_form.dtpEndTime.Value.Hour).AddMinutes(nt_form.dtpEndTime.Value.Minute);

                DevComponents.Schedule.Model.Appointment app = new DevComponents.Schedule.Model.Appointment(start, end, nt_form.txtName.Text + ", " + nt_form.txtType.Text);

                calendarView1.CalendarModel.Appointments.Add(app);
            }
        }

        private void calendarView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && calendarView1.DateSelectionStart == null)
            {
                DateTime start, end;
                calendarView1.GetDateSelectionFromPoint(new Point(e.X, e.Y), out start, out end);
                calendarView1.DateSelectionStart = start;
                calendarView1.DateSelectionEnd = end;
            }
        }

        private void AddAppointmentsToView(List<MyAppointment> list)
        {
            foreach (MyAppointment app in list)
            {
                calendarView1.CalendarModel.Appointments.Add(app);
            }
        }

        private void monthCalendarAdv1_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (e == null)
            {
                e = new DateRangeEventArgs(monthCalendarAdv1.SelectionStart, monthCalendarAdv1.SelectionEnd);
            }
            DateTime FormatStart = e.Start;

            calendarView1.CalendarModel.Appointments.Clear();
            if (calendarView1.SelectedView == DevComponents.DotNetBar.Schedule.eCalendarView.Day)
            {
                AddAppointmentsToView(Utils.getAppFromDB(FormatStart, FormatStart));
                calendarView1.DateSelectionStart = FormatStart;
            }
            else if (calendarView1.SelectedView == DevComponents.DotNetBar.Schedule.eCalendarView.Week)
            {
                DateTime temp = FormatStart;
                while (temp.DayOfWeek != DayOfWeek.Sunday)
                {
                    temp = temp.AddDays(-1);
                }
                AddAppointmentsToView(Utils.getAppFromDB(temp, temp.AddDays(6)));
                calendarView1.DateSelectionStart = temp;
                calendarView1.DateSelectionEnd = temp.AddDays(6);
            }
            else if (calendarView1.SelectedView == DevComponents.DotNetBar.Schedule.eCalendarView.Month)
            {
                DateTime temp = FormatStart;
                temp = e.Start.AddDays(-e.Start.Day + 1);
                AddAppointmentsToView(Utils.getAppFromDB(temp.AddDays(-7), temp.AddDays(DateTime.DaysInMonth(e.Start.Year, e.Start.Month) - 1).AddDays(7)));
                calendarView1.DateSelectionStart = temp;
                calendarView1.DateSelectionEnd = temp.AddDays(DateTime.DaysInMonth(e.Start.Year, e.Start.Month) - 1);
            }


            //calendarView1.Invalidate();
        }

        private void calendarView1_DateSelectionStartChanged(object sender, DevComponents.DotNetBar.Schedule.DateSelectionEventArgs e)
        {
            if (e.NewValue == null)
            {
                return;
            }
            DateTime newStart = (DateTime)e.NewValue;
            calendarView1.DayViewDate = newStart;
            calendarView1.WeekViewStartDate = newStart;
            calendarView1.MonthViewStartDate = newStart;
        }

        private void calendarView1_DateSelectionEndChanged(object sender, DevComponents.DotNetBar.Schedule.DateSelectionEventArgs e)
        {
            if (e.NewValue == null)
            {
                return;
            }
            DateTime newEnd = (DateTime)e.NewValue;
            calendarView1.DayViewDate = newEnd;
            calendarView1.WeekViewEndDate = newEnd;
            calendarView1.MonthViewEndDate = newEnd;
        }
    }
}
