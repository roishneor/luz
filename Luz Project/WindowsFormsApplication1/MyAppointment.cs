﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class MyAppointment: DevComponents.Schedule.Model.Appointment
    {
        public Brush ColorBlockBrush { get; set; }

        public string ClientName { get; set; }
        public int ID { get; set; }
        public string Treatment { get; set; }
        public string Comment { get; set; }
        public int Minutes { get; set; }
        public bool Payed { get; set; }
        public int PayedSum { get; set; }
        public DateTime SubmitDate { get; set; }
    }
}
